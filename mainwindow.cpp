#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    client1 = NULL;

    client1 = new QTcpSocket(this);

    connect(client1, SIGNAL(readyRead()),this, SLOT(readClient1()));

    client1->connectToHost(QHostAddress("127.0.0.1"),8001);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readClient1()
{
    char buffer[16048] = {0};
    client1->read(buffer, client1->bytesAvailable());
    QString data = QString::fromLocal8Bit(buffer);

    QJsonObject startObj = ObjectFromString(data);

    /*
    "ABSmagnitude":"3.44",
    "AZALT":"+184°12'43.8\"/+28°45'00.0",
    "AngSpeedPropMotion":"2648.0 (mas/yr)",
    "ApparentSidTime":"1h33m59.4s",
    "Distance":"24.33 ly",
    "EclDateLongLat":"+301°14'15.6\"/-64°47'44.1",
    "EclJ2000LongLat":"+300°56'54.3\"/-64°47'36.4",
    "EclOblDate":"+23°26'11.7",
    "GalLongLat":"-55°14'08.6\"/-39°46'13.6",
    "HADEC":"1h07m20.50s/-77°09'12.1",
    "IAU":"Hyi",
    "Magnitude":"2.80",
    "MeanSidTime":"1h34m00.4s",
    "MotionByAxes":"2579.3 599.2 (mas/yr)",
    "Parallax":"0.13407",
    "PosAngPropMotion":"76.9°",
    "RADECDate":"0h26m38.92s/-77°09'12.1",
    "RADECJ2000":"0h25m49.82s/-77°15'59.6",
    "SpectralType":"G0V",
    "SuperGalLongLat":"-55°14'08.6\"/-39°46'13.6",
    "Transit":"14h26m",
    "Type":"star",
    "name":"β Hyi - HIP 2021 - SAO 255670 - HD 2151 - HR 98"
    */

    ui->in_name->setText(startObj["name"].toString());

    qDebug() << startObj;
}

QJsonObject MainWindow::ObjectFromString(const QString& in)
{
    QJsonObject obj;

    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

    // check validity of the document
    if(!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
        }
        else
        {
            qDebug() << "Document is not an object" << endl;
        }
    }
    else
    {
        qDebug() << "Invalid JSON...\n" << in << endl;
    }

    return obj;
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QXmlStreamReader>
#include <QThread>
#include <QtNetwork>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonDocument>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void readClient1();
    QJsonObject ObjectFromString(const QString& in);

private:
    Ui::MainWindow *ui;
    QTcpServer server1;
    QTcpSocket *client1, *clientMain;
};

#endif // MAINWINDOW_H
